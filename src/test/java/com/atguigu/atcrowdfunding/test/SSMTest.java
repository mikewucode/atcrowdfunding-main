package com.atguigu.atcrowdfunding.test;

import com.atguigu.atcrowdfunding.mapper.TAdminMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

//项目启动时，web.xml中 DispatcherServlet和ContextLoaderListener负责容器对象的创建
@RunWith(SpringRunner.class) //指定使用spring-test 测试包中的测试类取代 junit的测试类
@ContextConfiguration(locations={"classpath:spring/spring-bean.xml",  //指定当前测试类的测试方法时创建spring容器需要使用的配置文件
        "classpath:spring/spring-mybatis.xml","classpath:spring/spring-tx.xml"})
public class SSMTest {
    //是从spring容器中获取mapper的对象进行自动装配
    @Autowired
    TAdminMapper adminMapper;
    @Test
    public void test1(){
        long count = adminMapper.countByExample(null);
        System.out.println("count = " + count);
    }
}
