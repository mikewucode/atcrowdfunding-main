package com.atguigu.atcrowdfunding.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class AppStartUpListener  implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        System.out.println("启动成功，存储路径。。。");
        servletContextEvent.getServletContext().setAttribute("PATH",
                servletContextEvent.getServletContext().getContextPath());
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        System.out.println("项目即将销毁，可以完成收尾操作。。。");
    }
}
