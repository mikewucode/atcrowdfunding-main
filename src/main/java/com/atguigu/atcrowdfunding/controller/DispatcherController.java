package com.atguigu.atcrowdfunding.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class DispatcherController {

    @RequestMapping("/index")
    public  String index(){

        return "index"; //视图解析器解析返回视图名称
    }


    @RequestMapping("/login.html")
    public  String login(){

        return "admins/login";
    }


}
