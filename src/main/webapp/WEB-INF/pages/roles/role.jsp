<%--
  Created by IntelliJ IDEA.
  User: Coco
  Date: 2020/7/25
  Time: 8:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

   <%@include file="/WEB-INF/pages/include/base_css.jsp"%>
    <style>
        .tree li {
            list-style-type: none;
            cursor:pointer;
        }
        table tbody tr:nth-child(odd){background:#F4F4F4;}
        table tbody td:nth-child(even){color:#C00;}
    </style>
</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <div><a class="navbar-brand" style="font-size:32px;" href="#">众筹平台 - 角色维护</a></div>
        </div>
     <%@include file="/WEB-INF/pages/include/manager_loginbar.jsp"%>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <%@include file="/WEB-INF/pages/include/manager_menu.jsp"%>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="glyphicon glyphicon-th"></i> 数据列表</h3>
                </div>
                <div class="panel-body">
                    <form class="form-inline" role="form" style="float:left;">
                        <div class="form-group has-feedback">
                            <div class="input-group">
                                <div class="input-group-addon">查询条件</div>
                                <input    name="condition"  class="form-control has-success" type="text" placeholder="请输入查询条件">
                            </div>
                        </div>
                        <button type="button"  id="queryBtn"  class="btn btn-warning"><i class="glyphicon glyphicon-search"></i> 查询</button>
                    </form>
                    <button id="batchDelRolesBtn"  type="button"  class="btn btn-danger" style="float:right;margin-left:10px;"><i class=" glyphicon glyphicon-remove"></i> 删除</button>
                    <button type="button" class="btn btn-primary" style="float:right;" id="addRoleModalBtn" ><i class="glyphicon glyphicon-plus"></i> 新增</button>
                    <br>
                    <hr style="clear:both;">
                    <div class="table-responsive">
                        <table class="table  table-bordered">
                            <thead>
                            <tr >
                                <th width="30">#</th>
                                <th width="30"><input type="checkbox"></th>
                                <th>名称</th>
                                <th width="100">操作</th>
                            </tr>
                            </thead>
                            <tbody>


                            </tbody>
                            <tfoot>
                            <tr >
                                <td colspan="6" align="center">
                                    <ul class="pagination">

                                    </ul>
                                </td>
                            </tr>

                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<%--  更新角色的模态框  默认不显示 --%>
<div class="modal fade" id="updateRoleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">角色更新</h4>
            </div>
            <div class="modal-body">
                <form>
                    <%--  更新时一定要通过隐藏域回显id --%>
                    <input type="hidden" name="id" value=""/>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">角色名:</label>
                        <input name="name" type="text" class="form-control" id="recipient-name">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <button type="button" id="updateRoleBtn" class="btn btn-primary">确定</button>
            </div>
        </div>
    </div>
</div>


<%--  新增角色的模态框  默认不显示 --%>
<div class="modal fade" id="addRoleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">角色新增</h4>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">角色名:</label>
                        <input name="name" type="text" class="form-control" id="recipient-name">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <button type="button" id="addRoleBtn" class="btn btn-primary">确定</button>
            </div>
        </div>
    </div>
</div>
<%@include file="/WEB-INF/pages/include/base_js.jsp"%>





<script type="text/javascript">



        // 批量删除按钮单击事件：
        $("#batchDelRolesBtn").click(function () {
            var roleidsArr = new Array();
            //获取所有被选中的子复选框所在行的角色id列表提交给后台删除
            if($("tbody :checkbox:checked").length==0){
                layer.msg("请选择要删除的角色");
                return;
            }
            $("tbody :checkbox:checked").each(function () {
                //this代表正在遍历的dom对象
                var roleid = this.id;
                roleidsArr.push(roleid);
            });




            //提交异步批量删除的请求
            $.ajax({
                "url":"${PATH}/role/batchDelRoles",
                "type":"get",
                "data":{"ids": roleidsArr.join(",")},
                "success":function (result) {
                    if("ok"==result){
                        //将被选中的子复选框所在的tr删除
                        $("tbody :checkbox:checked").parents("tr").remove();
                        //判断批量删除后如果有数据就dom删除删除掉的数据，如果没有数据刷新当前页
                        if($("tbody :checkbox").length==0){
                            getRoles(currentPageNum);
                        }
                    }
                }
            });

        });




    //全选全不选效果   事件处理函数由于是页面加载完成后用户操作标签才会被调用，所以函数中可以使用异步加载生成的标签
    $("thead :checkbox").click(function () {
        //设置tbody内的所有的复选框选中状态和它一致
        $("tbody :checkbox").prop("checked" , this.checked);
    });
    //$("tbody :checkbox")异步生成的标签，不能在页面加载时直接使用
    // $("tbody :checkbox").click(function () {
    //     alert("xxxx");
    // });
    $("tbody").delegate(":checkbox" , "click" , function () {
        //如果所有的子复选框都选中则设置全选框选中
        $("thead :checkbox").prop("checked" , $("tbody :checkbox").length==$("tbody :checkbox:checked").length);
    })

     //  提交更新请求
     $("#updateRoleModal  #updateRoleBtn").click(function(){

         $.post("${PATH}/role/update",$("#updateRoleModal form").serialize(),function (data) {

                if ("ok" ==data){

                    // 更新成功  关闭模态框 ，刷新当前页面
                    $("#updateRoleModal").modal("hide");
                    getRoles(currentPageNum  , $("form input[name='condition']").val());
                    layer.msg("更新成功");
                }
         });

     })







    //更新按钮的单击事件
    $("tbody").delegate(".updateRoleBtn" , "click" , function () {
        //alert($(this).attr("roleid"));
        //更新按钮所在行的角色id
        var roleid = $(this).attr("roleid");
        //发送异步请求获取要更新的角色信息回显到模态框中
        $.getJSON("${PATH}/role/role",{"id":roleid} , function (role) {
            //将role的json对象的数据回显到模态框中
            $("#updateRoleModal form input[name='id']").val(role.id);
            $("#updateRoleModal form input[name='name']").val(role.name);
            //在数据回显成功后显示模态框
            $("#updateRoleModal").modal("show");
        } );
    })





        // ================== 新增角色=========================
        $("#addRoleModalBtn").click(function(){

            // 显示模态框

            $("#addRoleModal").modal("toggle");

        });

        // 点击时提交新增请求

        //当新增模态框确认按钮点击，提交新增角色的信息到后台，保存成功后关闭模态框并刷新角色分页数据
        $("#addRoleModal #addRoleBtn").click(function () {
            $.ajax({
                "type":"post",
                "url":"${PATH}/role/save",
                "data": $("#addRoleModal form").serialize(),//获取表单内的有name属性值的表单项参数拼接[ name=xxx&pwd=xxx]
                "success":function (result) {
                    if(result=="ok"){
                        //新增成功，关闭模态框
                        $("#addRoleModal").modal("hide");
                        //加载最后一页的角色列表显示
                        getRoles(totalPages+1);//totalPages是我们声明的全局变量，在getRoles方法中初始化的总页码
                    }
                }
            });
        });

        // =================带条件的异步查询=======================
        $("form  #queryBtn").click(function(){

            // 获取条件
            var condition = $("form input[name='condition']").val();

           // 发送异步请求查询分页数据并显示到页面中
            getRoles(1,condition);

        });


        //当前页面已经加载结束，可以发送异步请求获取角色列表数据显示到当前页面中
        getRoles(1);
        //抽取ajax请求并解析角色分页数据的代码
        var  currentPageNum;
        var   totalPages;
        function getRoles(pageNum , condition){

            currentPageNum = pageNum;
            $.ajax({
                "type":"get",
                "url":"${PATH}/role/roles",
                "data":{"pageNum":pageNum ,"condition":condition},
                "success":function (pageInfo) {
                        totalPages = pageInfo.pages;
                    //删除上一次异步请求解析到页面中的dom对象
                    // remove():  删除标签
                    $("tbody").empty();//掏空tbody
                    $("tfoot ul").empty();
                    //打印接受到的服务器的相应结果： 角色列表的分页对象
                    console.log(pageInfo);
                    //遍历显示pageInfo的数据到页面中
                    initRoles(pageInfo);
                    //分页导航栏
                    initNavPages(pageInfo);

                }
            });
        }






        //将分页数据的分页导航栏遍历显示到页面中
        function initNavPages(pageInfo){
            //分页导航栏所有的超链接点击时都希望用js发送网络请求返回的结果用js代码解析展示到页面中
            //分页导航栏:js dom操作 解析分页数据生成分页导航栏标签追加到页面中
            //上一页
            if(pageInfo.isFirstPage){
                //没有上一页
                $('<li class="disabled"><a href="javascript:void(0);">上一页</a></li>').appendTo("tfoot ul");
            }else{
                $('<li><a class="navA" pagenum="'+pageInfo.prePage+'" href="javascript:void(0);">上一页</a></li>').appendTo("tfoot ul");
            }
            //中间页码
            $.each(pageInfo.navigatepageNums , function () {
                if(pageInfo.pageNum == this){
                    $('<li class="active"><a href="javascript:void(0);">'+ this +' <span class="sr-only">(current)</span></a></li>').appendTo("tfoot ul");
                }else{
                    $('<li><a class="navA" pagenum="'+this+'" href="javascript:void(0);">'+ this +'</a></li>').appendTo("tfoot ul");
                }
            });
            //下一页
            if(pageInfo.isLastPage){
                //没有下一页
                $('<li class="disabled"><a href="javascript:void(0);">下一页</a></li>').appendTo("tfoot ul");
            }else{
                $('<li><a class="navA" pagenum="'+ pageInfo.nextPage +'" href="javascript:void(0);">下一页</a></li>').appendTo("tfoot ul");
            }
            //当标签创建后页面中一定存在时再绑定单击事件
            //1-在标签创建的下面给超链接绑定单击事件
            $("tfoot ul .navA").click(function () {
                var pagenum = $(this).attr("pagenum");
                //分页页码被点击，发送异步请求获取分页对象显示到页面中
                //分页页码被点击，发送异步请求获取分页对象显示到页面中
                //考虑页面是不是带条件查询到数据对应的页码数据
                var condition = $("form input[name='condition']").val();
                getRoles(pagenum,condition);
                getRoles(pagenum);
            });
        }






        // 将解析分页数据中的角色集合的方法提取
        function initRoles(pageInfo){
            $("thread :checkbox").prop("checked",false);
            //role列表数据:遍历每个role都对应一行显示
            //function函数的参数：代表正在遍历元素的索引
            $.each(pageInfo.list , function (i) {//将参数1对应的数组集合进行遍历，每次遍历都使用正在遍历的元素调用一次function函数
                //this代表正在遍历的元素
                //将正在遍历的角色显示到一个tr中追加给tbody
                $(' <tr>\n' +
                    '                                <td>'+(i+1)+'</td>\n' +
                    '                                <td><input id="'+this.id+'" type="checkbox"></td>\n' +
                    '                                <td>'+ this.name +'</td>\n' +
                    '                                <td>\n' +
                    '                                    <button type="button" class="btn btn-success btn-xs"><i class=" glyphicon glyphicon-check"></i></button>\n' +
                    '                                    <button roleid="'+this.id+'" type="button" class="btn btn-primary btn-xs updateRoleBtn"><i class=" glyphicon glyphicon-pencil"></i></button>\n' +
                    '                                    <button roleid="'+this.id+'" type="button" class="btn btn-danger btn-xs delRoleBtn"><i class=" glyphicon glyphicon-remove"></i></button>\n' +
                    '                                </td>\n' +
                    '                            </tr>').appendTo("tbody");
            });


        }




            $("tbody").delegate(".delRoleBtn","click",function(){

              var roleid   =$(this).attr("roleid");
               var  $tr = $(this).parents("tr");


               $.ajax({

                   "type": "get",
                   "url":"${PATH}/role/delete",
                   "data":{"id":roleid},
                   "success":function(result){
                       if (result =='ok'){
                  //后台删除成功，前台页面也需要删除点击按钮所在行的角色信息

                        $tr.remove();

                   //     layer.msg("删除成功");
                        if ($("tbody  .delRoleBtn").length ==0){
                                layer.msg("当前页面没有数据,即将刷新");

                             setTimeout(function(){
                                 getRoles(currentPageNum,$("form imput[name='condition']").val());
                             },3000);
                        }
                       }

                   }

               });

            });





    /* 权限管理-用户维护模块高亮显示的js脚本
    编写js代码，当前user页面被访问时，需要设置左侧菜单栏 权限管理菜单栏自动展开 */
    $("a:contains('用户维护')").parents("ul:hidden").show();
    //直接显示权限管理的子菜单列表，会导致权限管理单击事件第一次失效，当子菜单列表显示时，移除权限管理的tree-closed class值
    $(".list-group-item:contains(' 权限管理 ')").toggleClass("tree-closed");
    $(".list-group-item a:contains('角色维护')").css("color","red");



    $(function () {
        $(".list-group-item").click(function(){
            if ( $(this).find("ul") ) {
                $(this).toggleClass("tree-closed");
                if ( $(this).hasClass("tree-closed") ) {
                    $("ul", this).hide("fast");
                } else {
                    $("ul", this).show("fast");
                }
            }
        });
    });

    $("tbody .btn-success").click(function(){
        window.location.href = "assignPermission.html";
    });
</script>
</body>
</html>
