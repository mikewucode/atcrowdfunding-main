<%--
  Created by IntelliJ IDEA.
  User: Coco
  Date: 2020/7/23
  Time: 16:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!---->
    <%@include file="/WEB-INF/pages/include/base_css.jsp"%>

    <style>
        .tree li {
            list-style-type: none;
            cursor:pointer;
        }
        table tbody tr:nth-child(odd){background:#F4F4F4;}
        table tbody td:nth-child(even){color:#C00;}
    </style>
</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <div><a class="navbar-brand" style="font-size:32px;" href="#">众筹平台 - 用户维护</a></div>
        </div>
        <%@include file="/WEB-INF/pages/include/manager_loginbar.jsp"%>

    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <%@include file="/WEB-INF/pages/include/manager_menu.jsp"%>
            
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="glyphicon glyphicon-th"></i> 数据列表</h3>
                </div>
                <div class="panel-body">
                    <form   action="${PATH}/admin/index" class="form-inline" role="form" style="float:left;">
                        <div class="form-group has-feedback">
                            <div class="input-group">
                                <div class="input-group-addon"  >查询条件</div>
                                <input  name="condition" value="${param.condition}" class="form-control has-success" type="text" placeholder="请输入查询条件">
                            </div>
                        </div>
                        <button type="submit" class="btn btn-warning"><i class="glyphicon glyphicon-search"></i> 查询</button>
                    </form>
                    <button  id="batchDel" type="button" class="btn btn-danger" style="float:right;margin-left:10px;"><i class=" glyphicon glyphicon-remove"></i> 删除</button>
                    <button type="button" class="btn btn-primary" style="float:right;" onclick="window.location.href='${PATH}/admin/add.html'"><i class="glyphicon glyphicon-plus"></i> 新增</button>
                    <br>
                    <hr style="clear:both;">
                    <div class="table-responsive">
                        <table class="table  table-bordered">
                            <thead>
                            <tr >
                                <th width="30">#</th>
                                <th width="30"><input type="checkbox"></th>
                                <th>账号</th>
                                <th>名称</th>
                                <th>邮箱地址</th>
                                <th width="100">操作</th>
                            </tr>
                            </thead>
                            <tbody>

                            <c:forEach  items="${pageInfo.list}" var="admin"  varStatus="vs">
                                <tr>
                                    <td>${vs.count}</td>
                                    <td><input  adminid ="${admin.id}"   type="checkbox"></td>
                                    <td>${admin.loginacct}</td>
                                    <td>${admin.username}</td>
                                    <td>${admin.email}</td>
                                    <td>
                                        <button type="button"  adminid="${admin.id}"  class="btn btn-success btn-xs"><i class=" glyphicon glyphicon-check"></i></button>
                                        <button type="button"  adminid="${admin.id}" class="btn btn-primary btn-xs"><i class=" glyphicon glyphicon-pencil"></i></button>
                                        <button type="button"  onclick="window.location='${PATH}/admin/delete?id=${admin.id}&pageNum=${pageInfo.pageNum}'" class="btn btn-danger btn-xs"><i class=" glyphicon glyphicon-remove"></i></button>
                                    </td>
                                </tr>
                            </c:forEach>


                            </tbody>
                            <tfoot>
                            <tr >
                                <td colspan="6" align="center">
                                    <ul class="pagination">
                                        <c:choose>
                                            <c:when test="${pageInfo.hasPreviousPage}">

                                                <!--有上一页-->
                                                <li><a href="${PATH}/admin/index?pageNum=${pageInfo.prePage}&condition=${param.condition}">上一页</a></li>
                                            </c:when>
                                            <c:otherwise>
                                                   <!--没有上一页-->
                                                <li class="disabled"><a href="javascript:void (0);">上一页</a></li>
                                            </c:otherwise>
                                        </c:choose>
                                        <c:forEach items="${pageInfo.navigatepageNums}" var="index">
                                                <c:choose>
                                                    <c:when test="${pageInfo.pageNum==index}">
                                                        <%-- 正在遍历当前页码 高亮显示--%>
                                                        <li class="active"><a href="javascript:void(0)">${index} <span class="sr-only">(current)</span></a></li>

                                                    </c:when>
                                                    <c:otherwise>
                                                        <li><a href="${PATH}/admin/index?pageNum=${index}&condition=${param.condition}">${index}</a></li>
                                                    </c:otherwise>
                                                </c:choose>
                                        </c:forEach>
                                        
                                            <!--下一页-->
                                        <c:choose>
                                            <c:when test="${pageInfo.hasNextPage}">

                                                <!--有下一页-->
                                                <li ><a href="${PATH}/admin/index?pageNum=${pageInfo.nextPage}&condition=${param.condition}">下一页</a></li>
                                            </c:when>
                                            <c:otherwise>
                                                <!--没有上一页-->
                                                <li class="disabled"><a href="javascript:void (0);">下一页</a></li>
                                            </c:otherwise>
                                        </c:choose>

                                    </ul>
                                </td>
                            </tr>

                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<%@include file="/WEB-INF/pages/include/base_js.jsp"%>
<script type="text/javascript">



      // 批量删除复选框的全选效果
      $("thead :checkbox "  ).click(function () {
            $("tbody :checkbox").prop("checked",this.checked);   // 设置
      });

      //提交批量删除请求的单击事件
      //批量删除按钮的单击事件
      $("#batchDel").click(function () {
          //获取要被删除的管理员的id列表[被选中的子复选框所在行的管理员id]
          var adminids = new Array();
          $("tbody :checkbox:checked").each(function () {
              var adminid = $(this).attr("adminid");
              adminids.push(adminid);
          });
          if(adminids.length==0){
              layer.msg("请选择要删除的管理员");
              return ;
          }
          window.location = "${PATH}/admin/batchDel?pageNum=${pageInfo.pageNum}&ids="+adminids.join();
      });







    /*  编写js代码，当前user页面被访问时，需要设置左侧菜单栏 权限管理菜单栏自动展开 */
    $("a:contains('用户维护')").parents("ul:hidden").show();
    //直接显示权限管理的子菜单列表，会导致权限管理单击事件第一次失效，当子菜单列表显示时，移除权限管理的tree-closed class值
    $(".list-group-item:contains(' 权限管理 ')").toggleClass("tree-closed");
    $(".list-group-item a:contains('用户维护')").css("color","red");

    $(function () {
        $(".list-group-item").click(function(){
            if ( $(this).find("ul") ) {
                $(this).toggleClass("tree-closed");
                if ( $(this).hasClass("tree-closed") ) {
                    $("ul", this).hide("fast");
                } else {
                    $("ul", this).show("fast");
                }
            }
        });
    });
    $("tbody .btn-success").click(function(){
        window.location.href = "assignRole.html";
    });
    $("tbody .btn-primary").click(function(){

        var ad = $(this).attr("adminid");


        window.location.href = "${PATH}/admin/edit.html?id="+ad+"&pageNum=${pageInfo.pageNum}";
    });
</script>
</body>
</html>

