<%--
  Created by IntelliJ IDEA.
  User: Coco
  Date: 2020/7/28
  Time: 21:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <%@include file="/WEB-INF/pages/include/base_css.jsp"%>
    <style>
        .tree li {
            list-style-type: none;
            cursor:pointer;
        }
    </style>
</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <div><a class="navbar-brand" style="font-size:32px;" href="#">众筹平台 - 许可维护</a></div>
        </div>
        <%@include file="/WEB-INF/pages/include/manager_loginbar.jsp"%>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <%@include file="/WEB-INF/pages/include/manager_menu.jsp"%>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

            <div class="panel panel-default">
                <div class="panel-heading"><i class="glyphicon glyphicon-th-list"></i> 权限菜单列表 <div style="float:right;cursor:pointer;" data-toggle="modal" data-target="#myModal"><i class="glyphicon glyphicon-question-sign"></i></div></div>
                <div class="panel-body">
                   <ul  class ="ztree" id="hello"></ul>
                </div>
            </div>
        </div>
    </div>
</div>



    <%-- 新增菜单的模态框 --%>
    <div class="modal fade" id="addChildMenuModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="exampleModalLabel">新增菜单</h4>
                </div>
                <div class="modal-body">
                    <form>
                        <%--  通过隐藏域设置新增菜单的父菜单id --%>
                        <input type="hidden" name="pid"/>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">菜单名称</label>
                            <input type="text" name="name" class="form-control" id="recipient-name">
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">菜单图标</label>
                            <input name="icon" class="form-control" id="message-text"></input>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                    <button type="button" id="addChildMenuBtn" class="btn btn-primary">新增</button>
                </div>
            </div>
        </div>
    </div>
</div>



<%-- 更新菜单的模态框 --%>
<div class="modal fade" id="updateMenuModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">更新菜单</h4>
            </div>
            <div class="modal-body">
                <form>
                    <%-- 通过隐藏域回显要更新的菜单的id --%>
                    <input type="hidden" name="id"/>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">菜单名称</label>
                        <input type="text" name="name" class="form-control" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="control-label">菜单图标</label>
                        <input name="icon" class="form-control" id="message-text"></input>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
                <button type="button" id="updateMenuBtn" class="btn btn-primary">更新</button>
            </div>
        </div>
    </div>
</div>
<%@include file="/WEB-INF/pages/include/base_js.jsp"%>
<script type="text/javascript">


          // 更新模态框的
          $("#updateMenuModal  #updateMenuBtn" ).click(function(){
              $.ajax({
                  "type":"post",
                  "url":"${PATH}/menu/update",
                  "data":$("#updateMenuModal  form").serialize(),
                  "success":function (result) {
                      if ("ok"==result){
                          $("#updateMenuModal").modal("hide");
                          initMenusTree();
                          layer.msg("更新成功");
                      }
                  }
              });
          });


           // 给新增模态框的提交按钮绑定单击事件：点击时提交请求，成功后关闭模态框并刷新菜单树
           $("#addChildMenuModal   #addChildMenuBtn ").click(function(){
               $.ajax({
                   "type":"post",
                   "url":"${PATH}/menu/save",
                   "data":$("#addChildMenuModal form").serialize(),
                   "success":function(result){
                       if ("ok"==result) {
                           $("$addChildMenuModal").modal("hide");
                           // 重新加载菜单树
                           initMenusTree();
                           layer.msg("新增菜单成功");
                       }

                   }

               });

           });

        // 按钮的增删改单击事件

        function addChildMenu(pid){

             //设置pid回显到模态框
              //  alert("1111");
            $("#addChildMenuModal form input[name='pid']").val(pid);
            // 当点击函数时，模态框弹出
            $("#addChildMenuModal").modal("show");

        }

        function updateMenu(id){
            // 异步请求获取要更新的菜单
            $.ajax({
                "type":"get",
                "url":"${PATH}/menu/menu",
                "data":{"id":id},
                "success":function(menu){
                    //将异步获取到的菜单回显到更新的模态框中显示模态框
                       $("#updateMenuModal  form input[name='id']").val(menu.id);
                       $("#updateMenuModal  form  input[name='name']").val(menu.name);
                       $("#updateMenuModal  form  input[name='icon']").val(menu.icon);
                       $("#updateMenuModal").modal("show");
                }

            });

        }


        function  deleteMenu(id,menuName){


            layer.confirm("你真的要删除《"+ menuName+"》吗？",{"icon":3,"title":"删除确认:"},function(){
                $.ajax({
                    "type":"get",
                    "url":"${PATH}/menu/delete",
                    "data":{"id":id},
                    "success":function (result) {
                        if ("ok"==result){
                            initMenusTree();
                            layer.msg("删除成功");
                        }
                    }



                });
            });

        }

        // 页面打开时需要加载一次
           initMenusTree();
        // 加载菜单树的方法
        function  initMenusTree(){

            $.ajax({
                "type":"get",
                "url":"${PATH}/menu/menus",
                "success":function (menus) {

                    // menus就是多个菜单的json数组
                    menus.push({"id":0,"name":"系统权限菜单","icon":"glyphicon glyphicon-list-alt"});
                    var setting ={

                        // 禁止菜单点击跳转

                        data:{
                            key: {
                                url: "xasdasdads"//指定menu任意不存在的属性
                            },
                            simpleData:{
                                enable:true,
                                pIdKey:"pid"// 指定菜单如何查找自己的父节点
                            }
                        },


                        view: {
                            addDiyDom: function (treeId , treeNode){
                                //treeNode:ztree创建的每个li节点对象[属性包括每个节点对应的menu属性,和ztree添加的属性：tId ztree节点id]
                                console.log(treeNode);
                                $("#"+treeNode.tId+"_ico").remove();//代表显示每个节点图标的span标签id值
                                //在菜单名称标签后添加一个按钮
                                //$("#"+treeNode.tId+"_span").after("<button class='btn btn-success'>点我啊</button>");
                                //在菜单名称标签前面添加一个标签用来显示标签的图标
                                $("#"+treeNode.tId+"_span").before("<span class='"+ treeNode.icon +"'></span>");
                            },
                            addHoverDom: function(treeId, treeNode){
                                var aObj = $("#" + treeNode.tId + "_a"); // tId = permissionTree_1, ==> $("#permissionTree_1_a")
                                aObj.attr("href", "javascript:;");
                                aObj.prop("target","");
                                if (treeNode.editNameFlag || $("#btnGroup"+treeNode.tId).length>0) return;
                                var s = '<span id="btnGroup'+treeNode.tId+'">';
                                if ( treeNode.level == 0 ) {
                                    //根节点 只能新增
                                    s += '<a  onclick="addChildMenu('+ treeNode.id +')" class="btn btn-info dropdown-toggle btn-xs" style="margin-left:10px;padding-top:0px;" href="javascript:void(0);" >&nbsp;&nbsp;<i class="fa fa-fw fa-plus rbg "></i></a>';
                                } else if ( treeNode.level == 1 ) {
                                    //枝节点：有子节点和没有子节点
                                    //更新
                                    s += '<a onclick="updateMenu('+ treeNode.id +')" class="btn btn-info dropdown-toggle btn-xs" style="margin-left:10px;padding-top:0px;"  href="javascript:void(0);" title="修改权限信息">&nbsp;&nbsp;<i class="fa fa-fw fa-edit rbg "></i></a>';
                                    if (treeNode.children.length == 0) {//枝节点没有子节点
                                        //treeNode.menus.length==0
                                        //删除
                                         s += '<a onclick="deleteMenu('+ treeNode.id +',\''+treeNode.name+'\')" class="btn btn-info dropdown-toggle btn-xs" style="margin-left:10px;padding-top:0px;" href="javascript:void(0);" >&nbsp;&nbsp;<i class="fa fa-fw fa-times rbg "></i></a>';
                                    }
                                    //新增
                                    s += '<a onclick="addChildMenu('+ treeNode.id +')" class="btn btn-info dropdown-toggle btn-xs" style="margin-left:10px;padding-top:0px;" href="javascript:void(0);" >&nbsp;&nbsp;<i class="fa fa-fw fa-plus rbg "></i></a>';
                                } else if ( treeNode.level == 2 ) {
                                    //叶子节点
                                    //更新
                                    s += '<a  onclick="updateMenu('+ treeNode.id +')" class="btn btn-info dropdown-toggle btn-xs" style="margin-left:10px;padding-top:0px;"  href="javascript:void(0);" title="修改权限信息">&nbsp;&nbsp;<i class="fa fa-fw fa-edit rbg "></i></a>';
                                    //删除按钮
                                    s += '<a onclick="deleteMenu('+ treeNode.id +',\''+treeNode.name+'\')" class="btn btn-info dropdown-toggle btn-xs" style="margin-left:10px;padding-top:0px;" href="javascript:void(0);">&nbsp;&nbsp;<i class="fa fa-fw fa-times rbg "></i></a>';
                                }

                                s += '</span>';
                                aObj.after(s);
                            },
                            removeHoverDom: function(treeId, treeNode){
                                $("#btnGroup"+treeNode.tId).remove();
                            }
                        }
                    };
                    var zNodes = menus ;
                    //初始化生成ztree树   返回ztree树的对象
                    //参数1：容器元素对象 ， 参数2：配置 ， 参数3：数据源
                    var $ztreeObj = $.fn.zTree.init( $("#hello") , setting  , zNodes );
                    //设置ztree树自动展开
                    $ztreeObj.expandAll(true);
                }

            });
        }


    $(function () {
        $(".list-group-item").click(function(){
            if ( $(this).find("ul") ) {
                $(this).toggleClass("tree-closed");
                if ( $(this).hasClass("tree-closed") ) {
                    $("ul", this).hide("fast");
                } else {
                    $("ul", this).show("fast");
                }
            }
        });

        /* 权限管理-用户维护模块高亮显示的js脚本
       编写js代码，当前user页面被访问时，需要设置左侧菜单栏 权限管理菜单栏自动展开 */
        $("a:contains('用户维护')").parents("ul:hidden").show();
        //直接显示权限管理的子菜单列表，会导致权限管理单击事件第一次失效，当子菜单列表显示时，移除权限管理的tree-closed class值
        $(".list-group-item:contains(' 权限管理 ')").toggleClass("tree-closed");
        $(".list-group-item a:contains('菜单维护')").css("color","red");
    });
</script>
</body>
</html>

