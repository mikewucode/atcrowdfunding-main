<%--
  Created by IntelliJ IDEA.
  User: Coco
  Date: 2020/7/23
  Time: 20:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="tree">
    <ul style="padding-left:0px;" class="list-group">
        <c:choose>
            <c:when test="${empty pmenus}">
                <%-- 没有查询到菜单集合 --%>
                <h4>查询菜单失败！</h4>
            </c:when>
            <c:otherwise>
                <c:forEach items="${pmenus}" var="pmenu">
                    <c:choose>
                        <c:when test="${empty pmenu.children}">
                            <%-- 正在遍历的父菜单没有子菜单集合 --%>
                            <li class="list-group-item tree-closed" >
                                <a href="${PATH}/${pmenu.url}"><i class="${pmenu.icon}"></i> ${pmenu.name}</a>
                            </li>
                        </c:when>
                        <c:otherwise>
                            <%-- 有子菜单集合 --%>
                            <li class="list-group-item tree-closed">
                                <span><i class="${pmenu.icon}"></i> ${pmenu.name} <span class="badge" style="float:right">${pmenu.children.size()}</span></span>
                                <ul style="margin-top:10px;display:none;">
                                    <c:forEach items="${pmenu.children}" var="cmenu">
                                        <li style="height:30px;">
                                            <a href="${PATH}/${cmenu.url}"><i class="${cmenu.icon}"></i> ${cmenu.name}</a>
                                        </li>
                                    </c:forEach>
                                </ul>
                            </li>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </c:otherwise>
        </c:choose>
    </ul>
</div>
