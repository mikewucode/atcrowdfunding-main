<%--
  Created by IntelliJ IDEA.
  User: Coco
  Date: 2020/7/23
  Time: 20:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--当前页面只会在其他页面的下面引入使用，由于其他页面在最上面已经静态包含了base_css，base_css中已经有base标签了--%>

<script src="jquery/jquery-2.1.1.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="script/docs.min.js"></script>
<script src="script/back-to-top.js"></script>
<script src="layer/layer.js"></script>
<script src="ztree/jquery.ztree.all-3.5.min.js"></script>