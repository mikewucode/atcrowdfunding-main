<%--
  Created by IntelliJ IDEA.
  User: Coco
  Date: 2020/7/23
  Time: 20:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div id="navbar" class="navbar-collapse collapse">
    <ul class="nav navbar-nav navbar-right">
        <li style="padding-top:8px;">
            <div class="btn-group">
                <button type="button" class="btn btn-default btn-success dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-user"></i> ${sessionScope.admin.loginacct} <span class="caret"></span>
                </button>
                <ul class="dropdown-menu" role="menu">
                    <li><a href="#"><i class="glyphicon glyphicon-cog"></i> 个人设置</a></li>
                    <li><a href="#"><i class="glyphicon glyphicon-comment"></i> 消息</a></li>
                    <li class="divider"></li>
                    <li><a class="logoutA" href="javascript:void(0);"><i class="glyphicon glyphicon-off"></i> 退出系统</a></li>
                </ul>
            </div>
        </li>
        <li style="margin-left:10px;padding-top:8px;">
            <button type="button" class="btn btn-default btn-danger">
                <span class="glyphicon glyphicon-question-sign"></span> 帮助
            </button>
        </li>
    </ul>
    <form class="navbar-form navbar-right">
        <input type="text" class="form-control" placeholder="查询">
    </form>
</div>
<script type="text/javascript">


     // 当文档加载结束后再执行当前代码
     //loginbar代码是在页面的中间静态包含，但是js代码中用到的layer和jquery都是在页面的最后面静态包含的

     window.onload = function(){
         // 注销按键的单击事件
         $(".logoutA").click(function () {
             layer.confirm("你真的要注销吗?" , {"title":"注销确认" ,"icon":3} , function () {
                 layer.closeAll();
                 //点击确定提交注销请求
                 window.location = "${PATH}/admin/logout";
             })
         });
     }



</script>