<%--
  Created by IntelliJ IDEA.
  User: Coco
  Date: 2020/7/23
  Time: 20:18
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<base href="${PATH}/static/">
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/carousel.css">
<link rel="stylesheet" href="css/login.css">
<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" href="css/doc.min.css">
<link rel="stylesheet" href="ztree/zTreeStyle.css">
